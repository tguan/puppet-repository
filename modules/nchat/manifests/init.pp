# == Class: nchat
#
# === Authors
#
# Thomas Lane <tlane@bcit.ca>
#
class nchat {
  package { 'nmap-ncat' :
	ensure => latest,
  }

  file { '/etc/systemd/system/nchat.service' :
    source => "puppet:///modules/nchat/nchat.service",
    require => Package['nmap-ncat']
  }
  
  service { 'nchat' :
    ensure => running,
    enable => true,
    require => File['/etc/systemd/system/nchat.service']
  }
}
