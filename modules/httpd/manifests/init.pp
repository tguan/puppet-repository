class httpd {
	package { 'httpd':
		ensure => installed,
	}

	service { 'httpd':
		ensure => running,
		enable => true,
		require => Package['httpd'],
	}

	file { '/var/www/html/index.html':
		content => template('httpd/index.html.erb'),
		notify => Service['httpd'],
	}
}
